<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Jamshidbek_Karimov1
  Date: 7/13/2023
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search Chat</title>
    <link rel="stylesheet" href="chatStyle.css">
    <style>
        img{
            width:100px;
            border-radius: 50px;
            float: left;
        }.online-status {
             color: green;
             font-weight: bold;
         }
    </style>
</head>
<body>

<form action="${pageContext.request.contextPath}/search" method="post">
  <input type="text" name="phoneNumber" placeholder="type phoneNumber">
  <input type="submit">
</form>

<c:choose>
    <c:when test="${empty pageContext.request.getAttribute('result')}">
        <p>${message}</p>
    </c:when>
    <c:otherwise>
        <c:forEach items="${users}" var="users">
        <form action="${pageContext.request.contextPath}/open-chat" method="get">
            <input type="hidden" name="receiverId" value="${result.id}">
            <div><div>
                <button type="submit">
                <img src="${users.avatar}" alt="jfyjju"/>
                ${result.name}
                    <span class="online-status">Online</span>
                </button>
            </div>
<%--            <button type="submit">${result.name}</button></div>--%>
        </form>
        </c:forEach>
    </c:otherwise>
</c:choose>

</body>
</html>
