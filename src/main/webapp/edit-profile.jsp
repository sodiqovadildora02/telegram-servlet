<%--
  Created by IntelliJ IDEA.
  User: MADINA
  Date: 7/18/2023
  Time: 9:46 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload Photo</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/edit-profile" method="post" enctype="multipart/form-data">
    <label for="img">Select image:</label>
    <input type="file" id="img" name="img" accept="image/*">
    <button type="submit" class="button">Submit</button>
</form>
<form action="/edit-profile" method="post">
    <input type="text"  name="username" placeholder="username" value="${user.username}">
    <input type="text"  name="bio" placeholder="bio" value="${user.bio}">
    <button type="submit" class="button">Submit</button>
</form>



<a href="${pageContext.request.contextPath}/message">Go back</a>

</body>
</html>
