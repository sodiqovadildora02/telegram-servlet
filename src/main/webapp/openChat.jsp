<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Jamshidbek_Karimov1
  Date: 7/13/2023
  Time: 11:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chat</title>
    <link rel="stylesheet" href="chatStyle.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

        img {
            width: 100px;
            border-radius: 50px;
            float: left;
        }

        .container {
            width: 500px;
            height: 600px;
        }

        /* Modal styles */
        .modal {
            display: none; /* Hide the modal by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top of everything */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgba(0, 0, 0, 0.4); /* Black background with transparency */
        }

        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 80%; /* 80% width */
        }

        .online-text {
            color: green;
        }

    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>



<button type="submit" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">

    <img src=${receiver.avatar} alt="jfyjju"/>
    ${receiver.name}
    <div class="small"><span class="fa fa-circle chat-online"><span class="online-text">Online</span></span></div>
</button>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p><img src=${receiver.avatar} alt="jfyjju"/></p>
                <p>Name :${receiver.name}</p>
                <p>Phone number : ${receiver.phoneNumber}</p>
                <p>Username : ${receiver.username}</p>
                <p> Bio : ${receiver.bio}</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <form action="/contact" method="post">
                    <input type="hidden" value="${receiver.id}" name="receiverId">
                    <button type="submit" class="btn btn-success">Add contact</button>
                </form>
            </div>
        </div>

    </div>
</div>


<div class="container">


    <div style="overflow-y: auto; height: 500px;">
        <form action="/message" method="post">
            <button type="submit">

                <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512">
                    <!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
                    <path d="M459.5 440.6c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4L288 214.3V256v41.7L459.5 440.6zM256 352V256 128 96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4l-192 160C4.2 237.5 0 246.5 0 256s4.2 18.5 11.5 24.6l192 160c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V352z"/>
                </svg>
            </button>
        </form>
        <form action="/chat_delete" method="post">
            <input type="hidden" name="chatId" value="${chat.id}">
            <button type="submit" class="btn btn-danger">Delete chat</button>
        </form>

        <c:forEach items="${messages}" var="message">
            <c:choose>
                <c:when test="${message.senderId != userId}">
                    <div class="message-blue">
                        <p>${message.text}</p>
                        <div class="message-timestamp-left">

                                ${message.createdDate}
                            <a href="/write?messageId=${message.id}&chatId=${message.chatId}&receiverId=${receiver.id}">🗑</a>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="message-orange">
                        <p>${message.text}</p>
                        <div class="message-timestamp-right">
                            <a href="/write?messageId=${message.id}&chatId=${message.chatId}&receiverId=${receiver.id}">🗑</a>

                                ${message.createdDate}

                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>

    <div>
        <form action="/write" method="post">
            <input type="hidden" name="chatId" value="${chat.id}">
            <input type="text" name="text" placeholder="type here">
            <input type="hidden" name="receiverId" value="${receiverId}">
            <button type="submit">Send</button>
            <input type="file" class="form-control d-none" id="customFile2"/>
        </form>
    </div>
</div>

</div>

</body>
</html>