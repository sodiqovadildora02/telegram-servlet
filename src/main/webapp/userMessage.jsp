<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 17.07.2023
  Time: 22:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User message</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        body {
            margin-top: 20px;
        }

        .chat-online {
            color: #0bf642
        }

        .chat-offline {
            color: #f8041d
        }

        .chat-messages {
            display: flex;
            flex-direction: column;
            max-height: 800px;
            overflow-y: scroll
        }

        .chat-message-left,
        .chat-message-right {
            display: flex;
            flex-shrink: 0
        }

        .chat-message-left {
            margin-right: auto
        }

        .chat-message-right {
            flex-direction: row-reverse;
            margin-left: auto
        }

        .py-3 {
            padding-top: 1rem !important;
            padding-bottom: 1rem !important;
        }

        .px-4 {
            padding-right: 1.5rem !important;
            padding-left: 1.5rem !important;
        }

        .flex-grow-0 {
            flex-grow: 0 !important;
        }

        .border-top {
            border-top: 1px solid #03f603 !important;

        }

        .fixed-div {
            display: inline-block;
            position: fixed;
            bottom: 50px;
            right: 50px;
            width: 70px;
            height: 70px;
            background-color: #61bd4f;
            border-radius: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 100;
            cursor: pointer;
        }

        .online-status {
            color: #21ee21;
            font-weight: bold;
        }

        img {
            width: 50px;
            border-radius: 50px;
            float: left;
        }


        #dropdownMenu2 {
            appearance: none;
            padding: 5px;
            background-color: #4834d4;
            color: #02fc3c;
            border: none;
            font-family: inherit;
            outline: none;
        }
    </style>
</head>
<body>
<main class="content">
    <div class="container p-0 mt-5" style="height: 700px">
        <h1 class="h3 mb-3">Private Messages</h1>
        <button><a href="index.jsp">
            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512">
                <!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
                <path d="M459.5 440.6c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4L288 214.3V256v41.7L459.5 440.6zM256 352V256 128 96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4l-192 160C4.2 237.5 0 246.5 0 256s4.2 18.5 11.5 24.6l192 160c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V352z"/>
            </svg>

        </a></button>
        <button type="submit" class="btn btn-info btn-lg" data-toggle="modal" data-target="#staticBackdrop">

            <img src=${currentUser.avatar} alt="jfyjju"/>
            ${currentUser.name}

            <div class="small"><span class="fas fa-circle chat-online online-indicator online-status "></span>
                Online
            </div>

        </button>

        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
             aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">🐧🐧🐧</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><img src=${currentUser.avatar} alt="jfyjju"/></p>
                        <p>Name :${currentUser.name}</p>
                        <p>Phone number :${currentUser.phoneNumber}</p>
                        <p>Username : ${currentUser.username}</p>
                        <p> Bio : ${currentUser.bio}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <%--                        <form action="${pageContext.request.contextPath}/edit-profile" method="post" enctype="multipart/form-data">--%>
                        <%--                            <label for="img">Select image:</label>--%>
                        <%--                            <input type="file" id="img" name="img" accept="image/*">--%>
                        <a href="/edit-profile" methods="get">
                            <button type="submit">
                                Edit profile
                            </button>
                        </a>
                        <%--                        <button class="button">Submit</button>--%>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="row g-0">
                <div class="col-12 col-lg-5 col-xl-3 border-right">

                    <div class="px-4 d-none d-md-block">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1">
                                <form action="${pageContext.request.contextPath}/search" method="post">
                                    <div class="flex-grow-1">
                                        <input
                                                name="phoneNumber"
                                                type="text"
                                                class="form-control my-3"
                                                placeholder="Search..."
                                        />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <c:choose>
                        <c:when test="${searchResult != null}">
                            <c:forEach items="${searchResult}" var="item">

                                <form action="${pageContext.request.contextPath}/open-chat" method="get">
                                    <div class="list-group-item list-group-item-action border-0">
                                        <div class="d-flex align-items-start">
                                            <button type="submit">
                                                <img src="${item.avatar}"
                                                     class="rounded-circle mr-1"
                                                     alt="men" width="40" height="40">
                                                <div class="flex-grow-1 ml-3">
                                                    <input type="hidden" name="receiverId" value="${item.id}">
                                                        ${item.name}
                                                    <span class="online-status">Online</span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </c:forEach>
                        </c:when>
                    </c:choose>


                    <c:choose>
                        <c:when test="${chats != null}">
                            <jsp:useBean id="chats"
                                         scope="request"
                                         type="java.util.List<com.example.telegramwithservletjsp.model.ChatInfo>"
                            />
                            <div style="overflow:scroll; height: 500px">
                                <c:forEach items="${chats}" var="item">
                                    <form action="${pageContext.request.contextPath}/open-chat" method="get">
                                        <div class="list-group-item list-group-item-action border-0">
                                            <div class="d-flex align-items-start">
                                                <img src="${item.avatar}"
                                                     class="rounded-circle mr-1"
                                                     alt="men" width="40" height="40">
                                                <div class="flex-grow-1 ml-3">

                                                    <input type="hidden" name="receiverId" value="${item.receiver_id}">
                                                    <button type="submit"> ${item.userName}</button>
                                                    <div class="small"><span class="fas fa-circle chat-online"></span>
                                                        Online
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </c:forEach>
                                <div class="fixed-div">
                                    <form action="/userContact" method="get">
                                    <button type = "submit" style="border: none; background-color: transparent;">
                                            📞✏️
                                    </button>
                                    </form>
                                </div>
                            </div>

                        </c:when>
                    </c:choose>
                    <hr class="d-block d-lg-none mt-1 mb-0">
                </div>
                <c:choose>
                    <c:when test="${messages != null}">
                        <div class="col-12 col-lg-7 col-xl-9">
                            <div class="py-2 px-4 border-bottom d-none d-lg-block">
                                <div class="d-flex align-items-center py-1">
                                    <div class="position-relative">
                                        <img src="https://scontent.ftas2-2.fna.fbcdn.net/v/t39.30808-1/355659555_976274147039189_8094137019942654428_n.jpg?stp=dst-jpg_p320x320&_nc_cat=102&ccb=1-7&_nc_sid=7206a8&_nc_ohc=KWs74TEXJrIAX-_CZqd&_nc_ht=scontent.ftas2-2.fna&oh=00_AfAtkDsCylvJGzfWJoVI6eFcKGBa2xod4nuXtWalMole7Q&oe=64B97B21"
                                             class="rounded-circle mr-1"
                                             alt="men"
                                             width="40"
                                             height="40">
                                    </div>

                                    <div class="flex-grow-1 pl-3">
                                        <strong>men</strong>
                                    </div>

                                    <div>
                                        <div class="dropdown">
                                            <button class="btn btn-light dropdown-toggle px-3 border btn-lg"
                                                    type="button"
                                                    id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false"
                                            >
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewbox="0 0 24 24"
                                                     fill="none" stroke="currentColor" stroke-width="2"
                                                     stroke-linecap="round"
                                                     stroke-linejoin="round"
                                                     class="feather feather-more-horizontal feather-lg">
                                                    <circle cx="12" cy="12" r="1"></circle>
                                                    <circle cx="19" cy="12" r="1"></circle>
                                                    <circle cx="5" cy="12" r="1"></circle>
                                                </svg>
                                            </button>
                                                <%--DONE   --%>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <form action="/chat-delete" method="post">
                                                    <input name="currentChatId" type="hidden" value="${chat.id}">
                                                    <button
                                                            class="dropdown-item text-danger"
                                                            type="submit"
                                                    >
                                                        Delete Chat
                                                    </button>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative" style="height: 500px">
                                <div class="chat-messages p-4">
                                    <c:forEach items="${messages}" var="message">
                                        <c:choose>
                                            <c:when test="${message.senderId == userId}">
                                                <div class="chat-message-right pb-4">
                                                    <div>
                                                        <img src="https://bootdey.com/img/Content/avatar/avatar1.png"
                                                             class="rounded-circle mr-1" alt="Chris Wood"
                                                             width="40"
                                                             height="40"
                                                        >
                                                        <div class="text-muted small text-nowrap mt-2">2:33 am</div>
                                                    </div>
                                                    <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
                                                        <div class="font-weight-bold mb-1">You</div>
                                                            ${message.text}
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="chat-message-left pb-4">
                                                    <div>
                                                        <img src="https://scontent.ftas2-2.fna.fbcdn.net/v/t39.30808-1/355659555_976274147039189_8094137019942654428_n.jpg?stp=dst-jpg_p320x320&_nc_cat=102&ccb=1-7&_nc_sid=7206a8&_nc_ohc=KWs74TEXJrIAX-_CZqd&_nc_ht=scontent.ftas2-2.fna&oh=00_AfAtkDsCylvJGzfWJoVI6eFcKGBa2xod4nuXtWalMole7Q&oe=64B97B21"
                                                             class="rounded-circle mr-1" alt="men" width="40"
                                                             height="40">
                                                        <div class="text-muted small text-nowrap mt-2">2:34 am</div>
                                                    </div>
                                                    <div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3">
                                                        <div class="font-weight-bold mb-1">
                                                            <c:choose>
                                                                <c:when test="${chat.user1_id != userId}">
                                                                    ${chat.user1_name}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    ${chat.user2_name}
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                            ${message.text}
                                                    </div>
                                                </div
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="flex-grow-0 py-3 px-4 border-top" style="margin-top: 50px;">
                                <div class="input-group">
                                    <form action="${pageContext.request.contextPath}/write" method="post">
                                        <input name="receiverId" value="${userId}" type="hidden"/>
                                        <input name="text" type="text" class="form-control"
                                               placeholder="Type your message">
                                        <button class="btn btn-primary" type="submit">Send</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:when>
                </c:choose>
            </div>
        </div>
    </div>
</main>


<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
<!-- Add Bootstrap JS (popper.js is required for dropdowns and other components) -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.min.js"></script>
</body>
</html>
