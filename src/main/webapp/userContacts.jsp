<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 21.07.2023
  Time: 0:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Contacts</title>
    <style>

        .online-status {
            color: #21ee21;
            font-weight: bold;
        }

        img {
            width: 50px;
            border-radius: 50px;
            float: left;
        }

    </style>
</head>

<body>
<div class="container p-0 mt-5" style="height: 700px">
    <h1 class="h3 mb-3">My contacts</h1>
    <div style="overflow:scroll; height: 500px">
        <form action="/message" method="post">
        <button type="submit" >

                <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512">
                    <!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
                    <path d="M459.5 440.6c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4L288 214.3V256v41.7L459.5 440.6zM256 352V256 128 96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4l-192 160C4.2 237.5 0 246.5 0 256s4.2 18.5 11.5 24.6l192 160c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V352z"/>
                </svg>

        </button>
        </form>

        <jsp:useBean id="contacts" scope="request" type="java.util.List"/>
        <c:forEach items="${contacts}" var="item">
            <form action="${pageContext.request.contextPath}/open-chat" method="get">
                <div class="list-group-item list-group-item-action border-0">
                    <div class="d-flex align-items-start">
                        <img src="${item.avatar}"
                             class="rounded-circle mr-1 img"

                             alt="men" width="40" height="40">
                        <div class="flex-grow-1 ml-3">

                            <input type="hidden" name="receiverId" value="${item.userId}">
                            <button type="submit"> ${item.name}
                                <div class="small"><span class="fas fa-circle chat-online"></span>
                                    Online
                                </div>
                            </button>
                            <button type="submit" >
                                <a href="/deleteContact?userId=${item.userId}">🗑</a>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </c:forEach>

    </div>
</div>


</body>
</html>
