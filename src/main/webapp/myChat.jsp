<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 16.07.2023
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My chat</title>
    <link rel="stylesheet" href="chatStyle.css">
    <style>
        img{
            width:100px;
            border-radius: 50px;
            float: left;
        }.online-status {
             color: green;
             font-weight: bold;
         }</style>
</head>
<body>

<form action="${pageContext.request.contextPath}/my-chats" method="get">
</form>

<div class="d-flex justify-content-center flex-row">
    <div class="p-2">
        <table class="table">
            <tbody>
            <jsp:useBean id="myChats" scope="request"
                         type="java.util.List<com.example.telegramwithservletjsp.model.ChatInfo>"/>
            <c:forEach items="${myChats}" var="pr">

                <tr>
                    <div>
                        <c:choose>
                            <c:when test="${pr.user1_id != userId}">
                                <form action="/open-chat">
                                   <input type="hidden" name="receiverId" value="${pr.user1_id}"/>
                                   <button type="submit">
                                       <img src= "${pr.avatar}" alt="Italian Trulli">
                                           ${pr.user1_name}
                                       <span class="online-status">Online</span>
                                   </button>
                               </form>

                            </c:when>
                            <c:otherwise>
                                <form action="/open-chat">
                                    <input type="hidden" name="receiverId" value="${pr.user2_id}"/>
                                    <button type="submit">
                                            ${pr.user2_name}
                                    </button>
                                </form>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
