<%--
  Created by IntelliJ IDEA.
  User: Jamshidbek_Karimov1
  Date: 7/13/2023
  Time: 10:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${user.name}</title>
</head>
<body>
<button><a href="index.jsp">
    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512">
        <!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
        <path d="M459.5 440.6c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4L288 214.3V256v41.7L459.5 440.6zM256 352V256 128 96c0-12.4-7.2-23.7-18.4-29s-24.5-3.6-34.1 4.4l-192 160C4.2 237.5 0 246.5 0 256s4.2 18.5 11.5 24.6l192 160c9.5 7.9 22.8 9.7 34.1 4.4s18.4-16.6 18.4-29V352z"/>
    </svg>
</a></button>

<div>
    <a href="search.jsp">
        <button>Search Chat</button>
    </a>
</div>
<form action="/message" method="post">
    <div>

        <button type="submit">My Chats</button>

    </div>
</form>
<div>
    <a href="/edit-profile" methods="get">
    <button type="submit">
Edit profile
</button></a>
</div>

</body>
</html>
