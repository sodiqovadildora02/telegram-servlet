<%--
  Created by IntelliJ IDEA.
  User: Jamshidbek_Karimov1
  Date: 4/17/2023
  Time: 4:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <style>
        body {
            /*background-image: url("https://googol.uz/storage/app/uploads/public/5e2/592/b8c/thumb_350_910_570_0_0_crop.jpg");*/

            webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background: white url("https://i.pinimg.com/originals/6a/b0/0b/6ab00b08fe476efaa30313455d42be8f.gif");
        }
    </style>
    <style> .container {
        font-align: center;
        height: 200px;
    }
    .vertical-center {
        margin: 0;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    /* Button styles */
    .button {
        padding: 10px 20px;
        background-color: #4CAF50;
        color: white;
        border: none;
        cursor: pointer;
    }

    .whole-container {
        text-align: center;
        margin-top: 100px;
    }
    .time {
        font-size: 120px;
        font-style: italic;
        font-family: "Verdana", sans-serif;
        text-transform:uppercase;
        background: linear-gradient(#67bb2f, #0f2fd5);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        /*margin-bottom: 50px;*/)
        /*text-align: center;*/
        /*font-family: sans-serif;*/
        /*letter-spacing: 0.15rem;*/
        /*text-transform: uppercase;*/
        /*color: #fff;*/
        /*text-shadow: -4px 4px #ef3550,*/
        /*-8px 8px #f48fb1,*/
        /*-12px 12px #7e57c2,*/
        /*-16px 16px #2196f3,*/
        /*-20px 20px #26c6da,*/
        /*-24px 24px #43a047,*/
        /*-28px 28px #eeff41,*/
        /*-32px 32px #f9a825,*/
        /*-36px 36px #ff5722;*/
    }
    </style>
</head>
<body>

<!-- Button to open the modal -->
<div class="whole-container">
    <span id="current-time" class="time"></span>
<script>


    function displayCurrentTime() {
        var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        var seconds = currentTime.getSeconds();

        // Add leading zeros if necessary
        hours = (hours < 10 ? "0" : "") + hours;
        minutes = (minutes < 10 ? "0" : "") + minutes;
        seconds = (seconds < 10 ? "0" : "") + seconds;

        var timeString = hours + ":" + minutes + ":" + seconds;

        // Update the HTML element
        document.getElementById("current-time").innerHTML = timeString;
    }

    // Update the time every second
    setInterval(displayCurrentTime, 1000);
</script>

<div>
                <a href="${pageContext.request.contextPath}/sign-up">
                    <button type="button" class="btn btn-outline-primary">Sign up</button>
                </a>
                <a href="${pageContext.request.contextPath}/sign-in">
                    <button type="button" class="btn btn-outline-warning">Sign in</button>
                </a>
</div>

</div>

</body>
</html>
