package com.example.telegramwithservletjsp.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BeanConfig {
    private static Connection connection;

    public static Connection connection() {

        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver");
                connection = DriverManager.getConnection(
                        "jdbc:postgresql://localhost:5432/TelegramWithServlet",
                        "postgres",
                        "2004"
                );

            } catch (ClassNotFoundException | SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return connection;

    }
}
