package com.example.telegramwithservletjsp.service.userservice;

import com.example.telegramwithservletjsp.exception.AuthException;
import com.example.telegramwithservletjsp.exception.BadRequestException;
import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.UUID;

import static com.example.telegramwithservletjsp.config.BeanConfig.connection;

public class UserServiceImpl implements UserService{
    private ArrayList<User> userList = new ArrayList<>();


//     {
//         User user1 = new User("asdf", "asdf", "asdf", "asdf", "asdf", "asdf");
//         user1.setId(UUID.fromString("7626e818-5515-4a41-b554-3f56f74bc298"));
//
//         User user2 = new User("qwer", "qwer", "qwer", "qwer", "qwer", "qwer");
//         user2.setId(UUID.fromString("7636e818-5515-4a41-b554-3f56f74bc298"));
//
//         User user3 = new User("1234", "1234", "1234", "1234", "1234", "1234");
//         user3.setId(UUID.fromString("7646e818-5515-4a41-b554-3f56f74bc298"));
//         userList.add(user1);
//         userList.add(user2);
//         userList.add(user3);
//    }
//    @Override
    public String save(User user) {
      return Util.userRepository.add(user);
    }
@Override
    public User signIn(String phoneNumber, String password) {
        return Util.userRepository.signIn(phoneNumber,password);
    }

    @Override
    public void update(UUID userId, String avatar) {
       Util.userRepository.update(userId,avatar);
    }

    @Override
    public void updateText(UUID id, String username, String bio) {
        Util.userRepository.updateText(id,username,bio);
    }

    @Override
    public User getById(UUID id) {
         return Util.userRepository.getById(id);
         }
    @Override
    public void checkPhoneNumber(String phoneNumber) {
        Util.userRepository.checkPhoneNumber(phoneNumber);
    }

    public ArrayList<User> findByPhoneNumber(String phoneNumber) {
       return Util.userRepository.findByPhoneNumber(phoneNumber);

      }
}
