package com.example.telegramwithservletjsp.service.userservice;

import com.example.telegramwithservletjsp.model.User;

import java.util.ArrayList;
import java.util.UUID;

public interface UserService {
    ArrayList<User> findByPhoneNumber(String phoneNumber);
    void checkPhoneNumber(String phoneNumber);
    User getById(UUID id);
    String save(User user);
    User signIn(String phoneNumber, String password);

    void update(UUID id, String s);


    void updateText(UUID id, String username, String bio);
}
