package com.example.telegramwithservletjsp.service.messageservice;


import com.example.telegramwithservletjsp.exception.ValidationException;
import com.example.telegramwithservletjsp.model.Message;
import com.example.telegramwithservletjsp.service.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MessageServiceImpl implements MessageService{
    private ArrayList<Message> messageList = new ArrayList<>();


    {
        Message message = new Message(UUID.fromString("7626e818-5515-4a41-b554-3f56f74bc298"), UUID.fromString("7726e818-5515-4a41-b554-3f56f74bc298"), "hello dear");
        Message message1 = new Message(UUID.fromString("7636e818-5515-4a41-b554-3f56f74bc298"), UUID.fromString("7726e818-5515-4a41-b554-3f56f74bc298"), "hi, do i know you??");
        messageList.add(message);
        messageList.add(message1);
    }
    @Override
    public String save(Message message) {
       return Util.messageRepository.add(message);
    }

    @Override
    public String delete(UUID id) {
      return Util.messageRepository.delete(id);
    }
@Override
    public List<Message> getChatMessages(UUID chatId) {
       return Util.messageRepository.getChatMessage(chatId);
    }
@Override
public void validateMessage(Message message) {
       Util.messageRepository.validateMessage(message);
    }
}
