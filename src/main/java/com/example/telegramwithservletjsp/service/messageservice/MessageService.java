package com.example.telegramwithservletjsp.service.messageservice;

import com.example.telegramwithservletjsp.model.Message;

import java.util.List;
import java.util.UUID;

public interface MessageService {
    void validateMessage(Message message);
    List<Message> getChatMessages(UUID chatId);
    String delete(UUID id);
    String save(Message message);

}
