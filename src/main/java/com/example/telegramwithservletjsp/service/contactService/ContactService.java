package com.example.telegramwithservletjsp.service.contactService;

import com.example.telegramwithservletjsp.model.Contact;
import com.example.telegramwithservletjsp.model.ContactInfo;

import java.util.ArrayList;
import java.util.UUID;

public interface ContactService {
    String addContact(Contact contact);
    ArrayList<ContactInfo> getAllContacts(UUID userId);
    String deleteContact(UUID currentUser , UUID userId);
}
