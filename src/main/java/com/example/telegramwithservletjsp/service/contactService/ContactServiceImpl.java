package com.example.telegramwithservletjsp.service.contactService;

import com.example.telegramwithservletjsp.model.Contact;
import com.example.telegramwithservletjsp.model.ContactInfo;
import com.example.telegramwithservletjsp.service.Util;
import com.example.telegramwithservletjsp.service.contactService.ContactService;

import java.util.ArrayList;
import java.util.UUID;

public class ContactServiceImpl implements ContactService {
    @Override
    public String addContact(Contact contact) {
        return Util.contactRepository.addContact(contact);
    }

    @Override
    public ArrayList<ContactInfo> getAllContacts(UUID userId) {
        return Util.contactRepository.getAll(userId);
    }

    @Override
    public String deleteContact(UUID currentUser, UUID userId) {
        return Util.contactRepository.deleteContact(currentUser, userId);
    }
}
