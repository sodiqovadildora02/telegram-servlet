package com.example.telegramwithservletjsp.service.chatservice;

import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;

import java.util.ArrayList;
import java.util.UUID;

public interface ChatService {
    Chat getChat(UUID senderId, UUID receiverId);
    public String save(Chat chat);

    ArrayList<ChatInfo> myChat(UUID userId);
    String deleteChat(UUID chatId);
}
