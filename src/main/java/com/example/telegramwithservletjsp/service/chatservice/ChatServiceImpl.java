package com.example.telegramwithservletjsp.service.chatservice;

import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.service.Util;

import java.util.ArrayList;
import java.util.UUID;

public class ChatServiceImpl implements ChatService {

    @Override
    public Chat getChat(UUID senderId, UUID receiverId) {
        return Util.chatrepository.getChat(senderId,receiverId);
    }

    @Override
    public String save(Chat chat) {
        return Util.chatrepository.add(chat);
    }

    @Override
    public ArrayList<ChatInfo> myChat(UUID userId) {
        return Util.chatrepository.myChat(userId);
    }

    @Override
    public String deleteChat(UUID chatId) {
        return Util.chatrepository.deleteChat(chatId);
    }
}