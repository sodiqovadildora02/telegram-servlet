package com.example.telegramwithservletjsp.service;

import com.example.telegramwithservletjsp.repository.chatrepository.ChatRepository;
import com.example.telegramwithservletjsp.repository.chatrepository.ChatRepositoryImpl;
import com.example.telegramwithservletjsp.repository.contactRepository.ContactRepository;
import com.example.telegramwithservletjsp.repository.contactRepository.ContactRepositoryImpl;
import com.example.telegramwithservletjsp.repository.messageservice.MessageRepository;
import com.example.telegramwithservletjsp.repository.messageservice.MessageRepositoryImpl;
import com.example.telegramwithservletjsp.repository.userrepository.UserRepository;
import com.example.telegramwithservletjsp.repository.userrepository.UserRepositoryImpl;
import com.example.telegramwithservletjsp.service.chatservice.ChatService;
import com.example.telegramwithservletjsp.service.chatservice.ChatServiceImpl;
import com.example.telegramwithservletjsp.service.contactService.ContactService;
import com.example.telegramwithservletjsp.service.contactService.ContactServiceImpl;
import com.example.telegramwithservletjsp.service.messageservice.MessageService;
import com.example.telegramwithservletjsp.service.messageservice.MessageServiceImpl;
import com.example.telegramwithservletjsp.service.userservice.UserService;
import com.example.telegramwithservletjsp.service.userservice.UserServiceImpl;

public interface Util {

    UserService userService = new UserServiceImpl();
    MessageService messageService = new MessageServiceImpl();
    ChatRepository chatrepository = new ChatRepositoryImpl();

    UserRepository userRepository = new UserRepositoryImpl();
    MessageRepository messageRepository = new MessageRepositoryImpl();

    ChatService chatService = new ChatServiceImpl();
    ContactRepository contactRepository = new ContactRepositoryImpl();
    ContactService contactService = new ContactServiceImpl();
}
