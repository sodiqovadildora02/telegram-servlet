package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.model.ContactInfo;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@WebServlet(name = "DeleteContact" , urlPatterns = "/deleteContact")
public class DeleteContact extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        Optional<Cookie> optionalCookie = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst();
        Cookie cookie = optionalCookie.get();
        String currentUser = cookie.getValue();

        String userId = req.getParameter("userId");

        String s = Util.contactService.deleteContact(UUID.fromString(currentUser), UUID.fromString(userId));


        ArrayList<ContactInfo> allContacts = Util.contactService.getAllContacts(UUID.fromString(currentUser));

        req.setAttribute("contacts",allContacts);

        req.getRequestDispatcher("userContacts.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
