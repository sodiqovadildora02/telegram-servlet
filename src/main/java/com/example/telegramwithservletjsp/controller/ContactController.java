package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.Contact;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.*;

@WebServlet(name = "Contact Controller", urlPatterns = "/contact")
public class ContactController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String phoneNumber = req.getParameter("phoneNumber");
        Cookie[] cookies = req.getCookies();
        String senderId = Arrays.stream(cookies).filter(c -> c.getName().equals("userId")).findFirst().map(Cookie::getValue).orElse("");
        String receiverId = req.getParameter("receiverId");

        System.out.println(receiverId);

        Contact contact = new Contact(UUID.fromString(senderId), UUID.fromString(receiverId));
        String s = Util.contactService.addContact(contact);

        User currentUser = userService.getById(UUID.fromString(senderId));

        User user = userService.getById(UUID.fromString(receiverId));

        req.setAttribute("receiver", user);
        req.setAttribute("userId", senderId);
        req.setAttribute("receiverId", receiverId);
        req.setAttribute("currentUser", currentUser);

        try {
            Chat chat = chatService.getChat(UUID.fromString(senderId), UUID.fromString(receiverId));
            if (chat == null) {
                req.setAttribute("messages", Collections.emptyList());
            } else {
                req.setAttribute("messages", messageService.getChatMessages(chat.getId()));
            }
            req.setAttribute("chat", chat);
        } catch (DataNotFoundException e) {
            req.setAttribute("messages", Collections.emptyList());
            req.setAttribute("chat", null);
        }

        req.getRequestDispatcher("openChat.jsp")
                .forward(req, resp);


    }
}
