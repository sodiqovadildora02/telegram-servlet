package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.model.ContactInfo;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@WebServlet(name = "User Contacts", urlPatterns = "/userContact")
public class UserContact extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        String senderId = Arrays.stream(cookies).filter(c -> c.getName().equals("userId")).findFirst().map(Cookie::getValue).orElse("");
        ArrayList<ContactInfo> allContacts = Util.contactService.getAllContacts(UUID.fromString(senderId));
        req.setAttribute("contacts",allContacts);
        req.getRequestDispatcher("userContacts.jsp").forward(req,resp);
    }
}
