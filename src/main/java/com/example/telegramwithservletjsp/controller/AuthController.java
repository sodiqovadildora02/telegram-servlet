package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import com.example.telegramwithservletjsp.service.chatservice.ChatService;
import com.example.telegramwithservletjsp.service.userservice.UserService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.telegramwithservletjsp.service.Util.userService;

@WebServlet(name = "auth", urlPatterns = {"/sign-up", "/sign-in"})
public class AuthController extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestURI = req.getRequestURI();

        if(requestURI.equals("/sign-up")) {
            resp.sendRedirect("signUp.jsp");
        } else {
            resp.sendRedirect("signIn.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(" i'm do post");
        String requestURI = req.getRequestURI();

        switch (requestURI) {
            case "/sign-in" -> {
                String phoneNumber = req.getParameter("phoneNumber");
                String password = req.getParameter("password");

                User user = userService.signIn(phoneNumber, password);
                if(user == null){
                    req.setAttribute("message", "user mpt found");
                    req.getRequestDispatcher("signIn.jsp").forward(req,resp);
                }else {
                    Cookie userId = new Cookie("userId", user.getId().toString());

                    resp.addCookie(userId);

                    req.setAttribute("user", user);
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("userMessage.jsp");
                    ArrayList<ChatInfo> chats = Util.chatService.myChat(user.id);
                    req.setAttribute("chats",chats);
                    req.setAttribute("currentUser",user);
                    requestDispatcher.forward(req, resp);
                }
            }
            case "/sign-up" -> {
                String phoneNumber = req.getParameter("phoneNumber");
                String password = req.getParameter("password");

                System.out.println(phoneNumber);
                System.out.println(password);

                String name = req.getParameter("name");
                User user = new User(name, null, password, phoneNumber, null, null);
                userService.save(user);
                resp.sendRedirect("index.jsp");

            }
        }
    }
}
