package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.model.Message;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.*;

@WebServlet(name = "Message Controller" , urlPatterns = "/message")
public class MessageController  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

/*
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = req.getParameter("text");

        String receiverId = req.getParameter("receiverId");
        String userId = "";

        req.setAttribute("receiverId", receiverId);
        for (Cookie cookie : req.getCookies()) {
            if (cookie.getName().equals("userId")) {
                userId = cookie.getValue();
                break;
            }
        }
        System.out.println(userId);
        System.out.println(receiverId);
        Chat chat = Util.chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));
        req.setAttribute("userId", userId);
        if (chat == null) {
            chat = new Chat();
            chat.setUser1Id(UUID.fromString(userId));
            chat.setUser2Id(UUID.fromString(receiverId));

            String save = Util.chatService.save(chat);
            chat = Util.chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));
        }
        Message message = new Message(UUID.fromString(userId), chat.getId(), text);

        String save = Util.messageService.save(message);

        List<Message> chatMessages = Util.messageService.getChatMessages(chat.getId());
        req.setAttribute("messages", chatMessages);

        req.getRequestDispatcher("openChat.jsp").forward(req, resp);
    }
*/

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = req.getParameter("text");

        String receiverId = req.getParameter("receiverId");
        String userId = "";
         req.setAttribute("searchResult",null);
        req.setAttribute("receiverId", receiverId);
        for (Cookie cookie : req.getCookies()) {
            if (cookie.getName().equals("userId")) {
                userId = cookie.getValue();
                break;
            }
        }
        User currentUser = userService.getById(UUID.fromString(userId));

        req.setAttribute("userId", userId);
        req.setAttribute("currentUser", currentUser);
        if (receiverId != null) {
            Chat chat = Util.chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));


            if (chat == null) {
                chat = new Chat();
                chat.setUser1Id(UUID.fromString(userId));
                chat.setUser2Id(UUID.fromString(receiverId));

                Util.chatService.save(chat);
                chat = Util.chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));
            }


            Message message = new Message(UUID.fromString(userId), chat.getId(), text);
            Util.messageService.save(message);

            List<Message> chatMessages = Util.messageService.getChatMessages(chat.getId());
            req.setAttribute("messages", chatMessages);
        }else{
            req.setAttribute("messages", null);
        }
            ArrayList<ChatInfo> list = Util.chatService.myChat(UUID.fromString(userId));
            req.setAttribute("chats", list);

            req.getRequestDispatcher("userMessage.jsp").forward(req, resp);

    }
}
