package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

@WebServlet(name = "Delete chat ", urlPatterns = "/chat_delete")
public class DeleteChat extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String chatId = req.getParameter("chatId");
        Cookie[] cookies = req.getCookies();

        String userId = "";
        for (Cookie c : cookies) {
            if (c.getName().equals("userId")) {
                userId = c.getValue();
                break;
            }
        }

        String s = Util.chatService.deleteChat(UUID.fromString(chatId));

        User currentUser = Util.userService.getById(UUID.fromString(userId));
        req.setAttribute("currentUser",currentUser);
        req.setAttribute("userId", userId.toString());

        ArrayList<ChatInfo> list = Util.chatService.myChat(UUID.fromString(userId));
        req.setAttribute("chats", list);

        req.getRequestDispatcher("userMessage.jsp").forward(req,resp);

    }
}
