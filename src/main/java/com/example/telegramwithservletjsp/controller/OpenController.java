package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.model.Message;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import com.example.telegramwithservletjsp.service.chatservice.ChatService;
import com.example.telegramwithservletjsp.service.messageservice.MessageService;
import com.example.telegramwithservletjsp.service.userservice.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.*;

@WebServlet(name = "Chat", urlPatterns = "/open-chat")
public class OpenController extends HttpServlet {
        @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        String senderId = "";
        for (Cookie c : cookies) {
            if (c.getName().equals("userId")) {
                senderId = c.getValue();
                break;
            }
        }
            User currentUser = userService.getById(UUID.fromString(senderId));

            String receiverId = req.getParameter("receiverId");

        User user = userService.getById(UUID.fromString(receiverId));
        req.setAttribute("receiver", user);
        req.setAttribute("userId", senderId);
        req.setAttribute("receiverId", receiverId);
        req.setAttribute("currentUser",currentUser);

        try {
            Chat chat = chatService.getChat(UUID.fromString(senderId), UUID.fromString(receiverId));
            if (chat == null) {
                req.setAttribute("messages", Collections.emptyList());
            } else {
                req.setAttribute("messages", messageService.getChatMessages(chat.getId()));
            }
            req.setAttribute("chat", chat);
        } catch (DataNotFoundException e) {
            req.setAttribute("messages", Collections.emptyList());
            req.setAttribute("chat", null);
        }

        req.getRequestDispatcher("openChat.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Salom men do post");
        Cookie[] cookies = req.getCookies();
        String senderId = "";
        for (Cookie c : cookies) {
            if (c.getName().equals("userId")) {
                senderId = c.getValue();
                break;
            }
        }
        String receiverId = req.getParameter("receiverId");
        User byId = userService.getById(UUID.fromString(senderId));

        Chat chat = chatService.getChat(byId.getId(), UUID.fromString(receiverId));
        List<Message> chatMessages = messageService.getChatMessages(chat.getId());

        req.setAttribute("chats",chat);
        req.setAttribute("user", byId);
        req.setAttribute("messages",chatMessages);
        req.getRequestDispatcher("openChat.jsp").forward(req,resp);
    }
}