package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.model.User;

import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.userService;

@MultipartConfig
        (
        maxFileSize = 1024*1024*10, // 10Mb
        fileSizeThreshold = 1024*20, // 20Kb (kilo bytes)
        maxRequestSize = 1024*1024*20 // 20Mb
)
@WebServlet(name = "Edit", urlPatterns = "/edit-profile")
public class EditProfileController extends HttpServlet {

    String UPLOAD_DIR = "C:\\Users\\HP\\Downloads\\Telegram Desktop\\TelegramWithServletJsp\\TelegramWithServletJsp\\src\\main\\webapp\\images";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        Optional<Cookie> optionalCookie = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst();
        Cookie cookie = optionalCookie.get();
        String userId = cookie.getValue();


        User user = userService.getById(UUID.fromString(userId));
        req.setAttribute("user", user);


        req.getRequestDispatcher("edit-profile.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        Optional<Cookie> optionalCookie = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst();
        Cookie cookie = optionalCookie.get();
        String userId = cookie.getValue();

        User user = userService.getById(UUID.fromString(userId));



        File fileSaveDir = new File(UPLOAD_DIR);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
        System.out.println("Upload File Directory="+fileSaveDir.getAbsolutePath());
        ArrayList<ChatInfo> chatInfos = Util.chatService.myChat(UUID.fromString(userId));
        req.setAttribute("chats",chatInfos);
        String username = req.getParameter("username");

        String bio = req.getParameter("bio");

        if (username == null) {
            String fileName = null;
            String saveFileName = null;
            for (Part part : req.getParts()) {
                fileName = getFileName(part);
                int index = fileName.lastIndexOf(".");
                saveFileName = userId + fileName.substring(index);
                part.write(UPLOAD_DIR + File.separator + saveFileName);
            }


            userService.update(user.getId(), "images/" + saveFileName);
        }else {
            userService.updateText(user.getId(), username, bio);

        }
        User currentUser = userService.getById(UUID.fromString(userId));
        req.setAttribute("currentUser", currentUser);

        req.getRequestDispatcher("userMessage.jsp").forward(req, resp);
    }
    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= "+contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
}
