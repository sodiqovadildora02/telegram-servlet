package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import com.example.telegramwithservletjsp.service.chatservice.ChatService;
import com.example.telegramwithservletjsp.service.userservice.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.userService;

@WebServlet(name = "search", urlPatterns = "/search")
public class SearchController extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String phoneNumber = req.getParameter("phoneNumber");
        Cookie[] cookies = req.getCookies();
        String senderId = Arrays.stream(cookies).filter(c -> c.getName().equals("userId")).findFirst().map(Cookie::getValue).orElse("");
        User currentUser = userService.getById(UUID.fromString(senderId));
        try {
            ArrayList<User> user = userService.findByPhoneNumber(phoneNumber);

            if (user == null){
                req.setAttribute("message","user not found");
            }else {
                req.setAttribute("searchResult", user.stream().filter(item-> !item.getId().equals(UUID.fromString(senderId))).toList());
                }

        } catch (DataNotFoundException e) {
            req.setAttribute("message", "user not found");

        }
         req.setAttribute("currentUser",currentUser );
        req.getRequestDispatcher("userMessage.jsp")
                .forward(req, resp);
    }

//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Cookie[] cookies = req.getCookies();
//        Optional<Cookie> first = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst();
//        Cookie cookie = first.get();
//        UUID userId = UUID.fromString(cookie.getValue());
//        req.setAttribute("userId", userId);
//
//        String phoneNumber = req.getParameter("phoneNumber");
//        try {
//            User user = userService.findByPhoneNumber(phoneNumber);
//            req.setAttribute("searchResult", user);
//        } catch (DataNotFoundException e) {
//            req.setAttribute("message", "user not found");
//        }
//
//        req.setAttribute("chats", null);
//        req.setAttribute("messages", null);
//        req.getRequestDispatcher("userMessage.jsp")
//                .forward(req, resp);
//    }
}
