package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.exception.ValidationException;
import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.Message;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.*;

@WebServlet(name = "Write Message", urlPatterns = "/write")
public class WriteMessage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        Optional<Cookie> optionalCookie = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst();
        Cookie cookie = optionalCookie.get();
        String userId = cookie.getValue();

        String messageId = req.getParameter("messageId");
        String chatId = req.getParameter("chatId");
        String receiverId = req.getParameter("receiverId");
        String delete = messageService.delete(UUID.fromString(messageId));
        User currentUser = userService.getById(UUID.fromString(userId));
        req.setAttribute("currentUser", currentUser);
        Chat chat = chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));

        User user = userService.getById(UUID.fromString(receiverId));
        req.setAttribute("receiver", user);
        req.setAttribute("userId", userId);

        req.setAttribute("messages", messageService.getChatMessages(chat.getId()));
        req.setAttribute("chat", chat);
        resp.sendRedirect(req.getContextPath()+"/open-chat?receiverId="+receiverId);}

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = req.getParameter("text");

        String receiverId = req.getParameter("receiverId");
        String userId = "";


        req.setAttribute("receiverId", receiverId);
        for (Cookie cookie : req.getCookies()) {
            if (cookie.getName().equals("userId")) {
                userId = cookie.getValue();
                break;
            }
        }
        System.out.println(userId);
        System.out.println(receiverId);
        User receiver = userService.getById(UUID.fromString(receiverId));
        req.setAttribute("receiver", receiver);

        Chat chat = chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));
        req.setAttribute("userId", userId);
        if (chat == null) {
            chat = new Chat();
            chat.setUser1Id(UUID.fromString(userId));
            chat.setUser2Id(UUID.fromString(receiverId));

            String save = chatService.save(chat);
            chat = chatService.getChat(UUID.fromString(userId), UUID.fromString(receiverId));
        }
        Message message = new Message(UUID.fromString(userId), chat.getId(), text);
       try {
           String save = messageService.save(message);
       }catch (ValidationException v){

       }


        List<Message> chatMessages = messageService.getChatMessages(chat.getId());
        req.setAttribute("chat",chat);

        req.setAttribute("messages", chatMessages);

        req.getRequestDispatcher("openChat.jsp").forward(req, resp);
    }
}
