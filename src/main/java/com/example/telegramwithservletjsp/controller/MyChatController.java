package com.example.telegramwithservletjsp.controller;

import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.model.User;
import com.example.telegramwithservletjsp.service.Util;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static com.example.telegramwithservletjsp.service.Util.userService;

@WebServlet(name = "My chat", urlPatterns = "/my_chats")
public class MyChatController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        Optional<Cookie> first = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst();
        Cookie cookie = first.get();
        UUID userId = UUID.fromString(cookie.getValue());

        User currentUser = userService.getById(userId); 
        req.setAttribute("currentUser", currentUser);

        ArrayList<ChatInfo> list = Util.chatService.myChat(userId);
        req.setAttribute("userId", userId.toString());
        req.setAttribute("myChats", list);
        req.getRequestDispatcher("myChat.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
