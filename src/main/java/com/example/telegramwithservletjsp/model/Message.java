package com.example.telegramwithservletjsp.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Message{
    public UUID id = UUID.randomUUID();
    public LocalDateTime createdDate = LocalDateTime.now();
    private LocalDateTime updatedDate = LocalDateTime.now();
    private UUID senderId;
    private UUID chatId;
    private String text;

    public Message(UUID senderId, UUID chatId, String text) {
        this.senderId = senderId;
        this.chatId = chatId;
        this.text = text;
    }
}
