package com.example.telegramwithservletjsp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Contact {
    private UUID id = UUID.randomUUID();
    private UUID user1id;
    private UUID user2id;

    public Contact(UUID senderId, UUID user2id) {
        this.user1id = senderId;
        this.user2id = user2id;
    }
}
