package com.example.telegramwithservletjsp.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class User{
    public UUID id = UUID.randomUUID();
    public LocalDateTime createdDate = LocalDateTime.now();
    private LocalDateTime updatedDate = LocalDateTime.now();
    private String name;
    private String username;
    private String password;
    private String phoneNumber;
    private String avatar;
    private String bio;
    private boolean isBlock = true;

    public User(String name, String username, String password, String phoneNumber, String avatar, String bio) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.avatar = avatar;
        this.bio = bio;
    }
}
