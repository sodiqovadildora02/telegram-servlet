package com.example.telegramwithservletjsp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ContactInfo {
    private UUID userId;
    private String name;
    private String avatar;
    private String phoneNumber;
}
