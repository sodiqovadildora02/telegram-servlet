package com.example.telegramwithservletjsp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ChatInfo {
    private  String userName;
    private String  avatar;
    private String phone_number;
    private UUID receiver_id ;
}
