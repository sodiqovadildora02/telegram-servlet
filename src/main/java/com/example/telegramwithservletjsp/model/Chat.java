package com.example.telegramwithservletjsp.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Chat{
    public UUID id = UUID.randomUUID();
    public LocalDateTime createdDate = LocalDateTime.now();
    private LocalDateTime updatedDate = LocalDateTime.now();
    private UUID user1Id;
    private UUID user2Id;
}
