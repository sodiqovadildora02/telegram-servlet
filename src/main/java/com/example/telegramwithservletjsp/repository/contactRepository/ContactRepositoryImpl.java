package com.example.telegramwithservletjsp.repository.contactRepository;

import com.example.telegramwithservletjsp.config.BeanConfig;
import com.example.telegramwithservletjsp.model.Contact;
import com.example.telegramwithservletjsp.model.ContactInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class ContactRepositoryImpl implements ContactRepository{
    private final Connection connection = BeanConfig.connection();


    @Override
    public String addContact(Contact contact) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from add_contact(?,?)");
            preparedStatement.setString(1, String.valueOf(contact.getUser1id()));
            preparedStatement.setString(2, String.valueOf(contact.getUser2id()));

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            return resultSet.getString("add_contact");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ArrayList<ContactInfo> getAll(UUID userId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from get_all_contacts(?)");
            preparedStatement.setString(1, String.valueOf(userId));
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<ContactInfo> list = new ArrayList<>();
            while (resultSet.next()){
                ContactInfo contactInfo = new ContactInfo();
                contactInfo.setAvatar(resultSet.getString("avatar"));
                contactInfo.setName(resultSet.getString("name"));
                contactInfo.setPhoneNumber(resultSet.getString("phoneNumber"));
                contactInfo.setUserId(UUID.fromString(resultSet.getString("userId")));
                list.add(contactInfo);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String deleteContact(UUID currentUser, UUID userId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from delete_contact(?,?)");
            preparedStatement.setString(1, String.valueOf(currentUser));
            preparedStatement.setString(2, String.valueOf(userId));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString("delete_contact");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
