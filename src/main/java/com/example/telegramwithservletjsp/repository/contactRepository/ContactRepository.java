package com.example.telegramwithservletjsp.repository.contactRepository;

import com.example.telegramwithservletjsp.model.Contact;
import com.example.telegramwithservletjsp.model.ContactInfo;

import java.util.ArrayList;
import java.util.UUID;

public interface ContactRepository {
    String addContact(Contact contact);

    ArrayList<ContactInfo> getAll(UUID userId);

    String deleteContact(UUID currentUser, UUID userId);
}
