package com.example.telegramwithservletjsp.repository.messageservice;

import com.example.telegramwithservletjsp.model.Message;

import java.util.List;
import java.util.UUID;

public interface MessageRepository {
    String add(Message message);

    String delete(UUID id);

    List<Message> getChatMessage(UUID chatId);

    void validateMessage(Message message);


}
