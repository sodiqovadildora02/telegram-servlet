package com.example.telegramwithservletjsp.repository.messageservice;

import com.example.telegramwithservletjsp.config.BeanConfig;
import com.example.telegramwithservletjsp.exception.ValidationException;
import com.example.telegramwithservletjsp.model.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MessageRepositoryImpl implements MessageRepository {
    private final Connection connection = BeanConfig.connection();


    @Override
    public String add(Message message) {
        validateMessage(message);

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from add_message(?,?,?)");
            preparedStatement.setString(1, String.valueOf(message.getSenderId()));
            preparedStatement.setString(2, String.valueOf(message.getChatId()));
            preparedStatement.setString(3, message.getText());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            return resultSet.getString("add_message");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public String delete(UUID messageId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from delete_message(?)");
            preparedStatement.setString(1, String.valueOf(messageId));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString("delete_message");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public List<Message> getChatMessage(UUID chatId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from get_chat_message(?)");
            preparedStatement.setString(1, String.valueOf(chatId));
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Message> list = new ArrayList<>();
            while (resultSet.next()) {
                Message message = Message.builder()
                        .chatId(UUID.fromString(resultSet.getString("chatid")))
                        .senderId(UUID.fromString(resultSet.getString("senderid")))
                        .id(UUID.fromString(resultSet.getString("id")))
                        .text(resultSet.getString("text"))
                        .createdDate(resultSet.getTimestamp("created_date").toLocalDateTime())
                        .updatedDate(resultSet.getTimestamp("updated_date").toLocalDateTime())
                        .build();
                list.add(message);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void validateMessage(Message message) {
        if (message.getText().isBlank()) {
            throw new ValidationException("message text cannot be empty");
        }
    }



//    public Message save(Message message) {
//        validateMessage(message);
//        messageList.add(message);
//        return message;
//    }
//    @Override
//    public String delete(UUID id) {
//        for (Message message : messageList) {
//            if(message.getId().equals(id)) {
//                messageList.remove(message);
//                return "message deleted";
//            }
//        }
//        return "message not found";
//    }
//    @Override
//    public List<Message> getChatMessages(UUID chatId) {
//        List<Message> messages = new ArrayList<>();
//
//        messageList.forEach((message) -> {
//            if(message.getChatId().equals(chatId))
//            {
//                messages.add(message);
//            }
//        });
//
//        return messages;
//    }
//    @Override
//    public void validateMessage(Message message) {
//        if(message.getText().length() == 0) {
//            throw new ValidationException("message text cannot be empty");
//        }
//    }
}
