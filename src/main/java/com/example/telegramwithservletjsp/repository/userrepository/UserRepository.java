package com.example.telegramwithservletjsp.repository.userrepository;

import com.example.telegramwithservletjsp.model.Message;
import com.example.telegramwithservletjsp.model.User;

import java.util.ArrayList;
import java.util.UUID;

public interface UserRepository {
    ArrayList<User> findByPhoneNumber(String phoneNumber);
    void checkPhoneNumber(String phoneNumber);
    User getById(UUID id);
    User signIn(String phoneNumber, String password);

    String add(User user);


    void update(UUID userId, String avatar);

    void updateText(UUID id, String username, String bio);
}
