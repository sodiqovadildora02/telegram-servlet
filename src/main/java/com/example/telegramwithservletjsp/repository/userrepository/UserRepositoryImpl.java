package com.example.telegramwithservletjsp.repository.userrepository;

import com.example.telegramwithservletjsp.config.BeanConfig;
import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository {
    private final Connection connection = BeanConfig.connection();

    @Override
    public ArrayList<User> findByPhoneNumber(String phoneNumber) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from find_by_phone_number(?)");
            preparedStatement.setString(1, phoneNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<User> list = new ArrayList<>();
            while (resultSet.next()) {
                User user = User.builder()
                        .name(resultSet.getString("name"))
                        .password(resultSet.getString("password"))
                        .phoneNumber(resultSet.getString("phone_number"))
                        .username(resultSet.getString("username"))
                        .id(UUID.fromString(resultSet.getString("id")))
                        .isBlock(resultSet.getBoolean("is_block"))
                        .bio(resultSet.getString("bio"))
                        .avatar(resultSet.getString("avatar"))
                        .createdDate(resultSet.getTimestamp("created_date").toLocalDateTime())
                        .updatedDate(resultSet.getTimestamp("updated_date").toLocalDateTime())
                        .build();

                list.add(user);
            }

            if (list.isEmpty()) {
                throw new DataNotFoundException("User not found");
            } else {
                return list;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void checkPhoneNumber(String phoneNumber) {

    }

    @Override
    public User getById(UUID userId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from get_by_id(?)");
            preparedStatement.setString(1, String.valueOf(userId));

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                User user = User.builder()
                        .name(resultSet.getString("name"))
                        .password(resultSet.getString("password"))
                        .phoneNumber(resultSet.getString("phone_number"))
                        .username(resultSet.getString("username"))
                        .id(UUID.fromString(resultSet.getString("id")))
                        .isBlock(resultSet.getBoolean("is_block"))
                        .bio(resultSet.getString("bio"))
                        .avatar(resultSet.getString("avatar"))
                        .createdDate(resultSet.getTimestamp("created_date").toLocalDateTime())
                        .updatedDate(resultSet.getTimestamp("updated_date").toLocalDateTime())
                        .build();
                user.setId(UUID.fromString(resultSet.getString("id")));
                return user;
            }
            throw new DataNotFoundException("User not found");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public User signIn(String phoneNumber, String password) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from sign_in(?,?)");
            preparedStatement.setString(1, password);
            preparedStatement.setString(2, phoneNumber);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                User user = User.builder()
                        .name(resultSet.getString("name"))
                        .password(resultSet.getString("password"))
                        .phoneNumber(resultSet.getString("phone_number"))
                        .username(resultSet.getString("username"))
                        .id(UUID.fromString(resultSet.getString("id")))
                        .isBlock(resultSet.getBoolean("is_block"))
                        .bio(resultSet.getString("bio"))
                        .avatar(resultSet.getString("avatar"))
                        .createdDate(resultSet.getTimestamp("created_date").toLocalDateTime())
                        .updatedDate(resultSet.getTimestamp("updated_date").toLocalDateTime())
                        .build();
                return user;
            }
            throw new DataNotFoundException("User not found or wrong input");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String add(User user) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from add_user(?,?,?,?,?,?)");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getUsername());
            preparedStatement.setString(3, user.getPhoneNumber());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getBio());
            preparedStatement.setString(6, user.getAvatar());


            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();

            return resultSet.getString("add_user");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(UUID userId, String avatar) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from update_profile(?,?)");
            preparedStatement.setString(1, String.valueOf(userId));
            preparedStatement.setString(2, avatar);


            boolean execute = preparedStatement.execute();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateText(UUID id, String username, String bio) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from update_text(?,?,?)");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.setString(2, username);
            preparedStatement.setString(3,bio);


            boolean execute = preparedStatement.execute();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
