package com.example.telegramwithservletjsp.repository.chatrepository;

import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;

import java.util.ArrayList;
import java.util.UUID;

public interface ChatRepository {
    String add(Chat chat);
    Chat getChat(UUID senderId, UUID receiverId);

    ArrayList<ChatInfo> myChat(UUID userId);

    String deleteChat(UUID chatId);
}
