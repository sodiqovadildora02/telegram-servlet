package com.example.telegramwithservletjsp.repository.chatrepository;

import com.example.telegramwithservletjsp.config.BeanConfig;
import com.example.telegramwithservletjsp.exception.DataNotFoundException;
import com.example.telegramwithservletjsp.model.Chat;
import com.example.telegramwithservletjsp.model.ChatInfo;
import com.example.telegramwithservletjsp.service.chatservice.ChatService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class ChatRepositoryImpl implements ChatRepository {

    private final Connection connection = BeanConfig.connection();


    @Override
    public String add(Chat chat) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from add_chat(?,?)");
            preparedStatement.setString(1, String.valueOf(chat.getUser1Id()));
            preparedStatement.setString(2, String.valueOf(chat.getUser2Id()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString("add_chat");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public Chat getChat(UUID senderId, UUID receiverId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from get_chat(?,?)");
            preparedStatement.setString(1, String.valueOf(senderId));
            preparedStatement.setString(2, String.valueOf(receiverId));
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Chat chat = Chat.builder()
                        .id(UUID.fromString(resultSet.getString("id")))
                        .user1Id(UUID.fromString(resultSet.getString("user1id")))
                        .user2Id(UUID.fromString(resultSet.getString("user2id")))
                        .createdDate(resultSet.getTimestamp("created_date").toLocalDateTime())
                        .updatedDate(resultSet.getTimestamp("updated_date").toLocalDateTime())
                        .build();
                return chat;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ArrayList<ChatInfo> myChat(UUID userId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from my_chat(?)");
            preparedStatement.setString(1, String.valueOf(userId));
            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<ChatInfo> chats = new ArrayList<>();

            while (resultSet.next()) {
                ChatInfo chat = ChatInfo.builder()
                        .userName(resultSet.getString("name"))
                        .phone_number(resultSet.getString("phone_number"))
                        .receiver_id(UUID.fromString(resultSet.getString("receiver_id")))
                        .avatar(resultSet.getString("avatar"))
                        .build();
                chats.add(chat);
            }
            return chats;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public String deleteChat(UUID chatId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from delete_chat(?)");
            preparedStatement.setString(1, String.valueOf(chatId));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getString("delete_chat");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}

